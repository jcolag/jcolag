# Hi, I'm John 👋 (he/him)

If you want to know more (maybe *too* much) about me, you should check out [my website](https://john.colagioia.net/) for links onward, including [my blog, **Entropy Arbitrage**](https://john.colagioia.net/blog).  But the short version is that, in my spare time, I work on projects that amuse me, whether it's learning about some corner of technology, prototyping new ideas, or just writing out what might be useful later on down the line.

Here's a sample of topics I've been talking about, this past week or so on the blog.

|Title|Date|
|-----|-------|
|[Developer Journal, Washington’s Farewell Address](https://john.colagioia.net/blog/2022/09/19/washington.html)|Mon Sep 19 2022|
|[Free Culture Book Club — Quand manigancent les haricots pt 3](https://john.colagioia.net/blog/2022/09/17/haricots-3.html)|Sat Sep 17 2022|
|[Tweets from 09/12 to 09/16](https://john.colagioia.net/blog/2022/09/16/week.html)|Fri Sep 16 2022|
|[Real Life in Star Trek, Home Soil](https://john.colagioia.net/blog/2022/09/15/home-soil.html)|Thu Sep 15 2022|
|[Developer Journal, María de Zayas](https://john.colagioia.net/blog/2022/09/12/dezayas.html)|Mon Sep 12 2022|

Feel free to suggest ideas on any project or just poke at me to say "hi." Not literally poke, though. Have some self-respect.

Here are some visualizations mostly for my purposes, which you can feel free to skip, especially the view counter that's probably mostly me making sure that I committed something.

![https://github-readme-streak-stats.herokuapp.com/demo/](https://github-readme-streak-stats.herokuapp.com/?user=jcolag "Streaks")
![https://github.com/anuraghazra/github-readme-stats](https://github-readme-stats.vercel.app/api/top-langs?username=jcolag&show_icons=true&locale=en&layout=compact&exclude_repo=ComicScanner,bisheng&langs_count=8 "Top languages")
![https://github.com/anuraghazra/github-readme-stats](https://github-readme-stats.vercel.app/api?username=jcolag&show_icons=true&locale=en "Statistics")
![https://github.com/ryo-ma/github-profile-trophy](https://github-profile-trophy.vercel.app/?username=jcolag&rank=SSS,SS,S,AAA,AA,A,SECRET,UNKNOWN "Trophies")
![https://github.com/antonkomarev/github-profile-views-counter](https://komarev.com/ghpvc/?username=jcolag "Profile views since 2021 June 07 or so; safe to ignore")

Mostly, I just missed the streak counter from years past.  I may eventually pull them from here and put them on the "dashboard page" that I generate and load every morning.

And while I don't expect it, if you would like to help fund some of this weirdness, I won't turn away anyone who wants to...

[<img src="images/default-yellow.png" alt="Buy Me a Coffee" width="150px"/>](https://www.buymeacoffee.com/jcolag)
